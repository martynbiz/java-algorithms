package codegolf;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @see https://codegolf.stackexchange.com/questions/93027/how-long-is-a-welsh-word
 * @author yqb18196
 *
 */
public class WelshWordCharacterCount {

	final String[] VALID_CHARS = { 
			
		
		// The following are all single letters in Welsh:
		"ch", "dd",  "ff", "ng", "ll", "ph", "rh", "th",
		"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "l", "m", "n", "o", "p", "r", "s", "t", "u", "w", "y",

		// These letters also exist in Welsh", "though they are restricted to technical vocabulary borrowed from other languages:
		"k", "q", "v", "x", "z",

		// Letters with diacritics are not regarded as separate letters", "but your function must accept them and be able to count them. Possible such letters are:
		"â", "ê", "î", "ô", "û", "ŷ", "ŵ", "á", "é", "í", "ó", "ú", "ý", "ẃ", "ä", "ë", "ï", "ö", "ü", "ÿ", "ẅ", "à", "è", "ì", "ò", "ù", "ẁ"
	
	};
	
	public int countCharacters(String word) throws Exception {
		
		int count = 0;
		
		// loop through each character and attempt to match from word 
		whileloop:
		while (word.length() > 0) {
			for(int i= 0; i< VALID_CHARS.length; i++) {
				String ch = VALID_CHARS[i];
				if (word.toLowerCase().startsWith(ch)) {
					
					// reduce word to remove the matched characters
					word = word.substring(ch.length());
					
					// increment count
					count++;
					
					continue whileloop;
				}
			}
			
			// if we reach this far, we are unable to match chars
			count = -1;
			break whileloop;
		}
		
		return count;
	}
}
