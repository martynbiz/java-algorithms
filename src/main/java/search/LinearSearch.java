package search;

public class LinearSearch {

	public int search(int[] array, int targetValue) {
		
		for (int guess=0; guess<array.length; guess++) {
			if (array[guess] == targetValue) {
				return guess;
			}
		}
		
		return -1;
	}
}
