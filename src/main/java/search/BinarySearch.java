package search;

public class BinarySearch {

	public int search(int[] array, int targetValue) {

		int start = 0;
		int end = array.length - 1;
		int guess;
		//{1,2,3,4,5,6,7,8,9}; <-- 1
		//{0,1,2,3,4,5,6,7,8};
		while (start <= end) {
			guess = (int) Math.floor((start + end) / 2);
			if (array[guess] == targetValue) {
				return guess;
			} else if (array[guess] > targetValue) {
				end = guess - 1;
			} else {
				start = guess + 1;
			}
		}

		return -1;
	}

}
