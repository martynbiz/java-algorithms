package sort;

public class InsertionSort {

	public int[] ascending(int[] array) {
		
		int temp;
		for (int i=0; i<array.length; i++) {
			for (int j=i; j>0; j--) {
				if (array[j] < array[j-1]) {
					temp = array[j];
					array[j] = array[j-1];
					array[j-1] = temp;
				}
			}
		}
		
		return array;
	}

	public int[] descending(int[] array) {
		
		return array;
	}
}
