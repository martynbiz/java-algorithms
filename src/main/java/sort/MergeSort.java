package sort;

public class MergeSort { 
  
    // Main function that sorts arr[l..r] using 
    // merge() 
    public void sort(int array[]) { 
        
    	int left = 0;
        int right = array.length - 1;
    	
    	mergeSort(array, left, right);
    } 
  
    // Main function that sorts arr[l..r] using 
    // merge() 
    void mergeSort(int arr[], int left, int right) { 
        
    	if (left < right) { 
            // Find the middle point 
            int middle = (left + right)/2; 
  
            // Sort first and second halves 
            mergeSort(arr, left, middle); 
            mergeSort(arr , middle+1, right); 
  
            // Merge the sorted halves 
            merge(arr, left, middle, right); 
        } 
    } 

	// Merges two subarrays of arr[]. 
    // First subarray is arr[l..m] 
    // Second subarray is arr[m+1..r] 
    void merge(int arr[], int left, int middle, int right) { 
        
    	// Find sizes of two subarrays to be merged 
        int n1 = middle - left + 1; 
        int n2 = right - middle; 
  
        /* Create temp arrays */
        int leftArray[] = new int [n1]; 
        int rightArray[] = new int [n2]; 
  
        /*Copy data to temp arrays*/
        for (int i=0; i<n1; ++i) 
        	leftArray[i] = arr[left + i]; 
        for (int j=0; j<n2; ++j) 
        	rightArray[j] = arr[middle + 1+ j]; 
  
  
        /* Merge the temp arrays */
  
        // Initial indexes of first and second subarrays 
        int i = 0, j = 0; 
  
        // Initial index of merged subarry array 
        int k = left; 
        while (i < n1 && j < n2) { 
            if (leftArray[i] <= rightArray[j]) { 
                arr[k] = leftArray[i]; 
                i++; 
            } else { 
                arr[k] = rightArray[j]; 
                j++; 
            } 
            k++; 
        } 
  
        /* Copy remaining elements of L[] if any */
        while (i < n1) { 
            arr[k] = leftArray[i]; 
            i++; 
            k++; 
        } 
  
        /* Copy remaining elements of R[] if any */
        while (j < n2) { 
            arr[k] = rightArray[j]; 
            j++; 
            k++; 
        } 
    } 
} 
