package codegolf;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Before;
import org.junit.jupiter.api.Test;

class WelshWordCharacterCountTest {

	@Test
	void testCountCharactersReturnsCorrectValue() throws Exception {
		
		WelshWordCharacterCount wordCount = new WelshWordCharacterCount();
		
		assertEquals(8, wordCount.countCharacters("Llandudno"));
		assertEquals(6, wordCount.countCharacters("Llanelli"));
		assertEquals(3, wordCount.countCharacters("Rhyl"));
		assertEquals(50, wordCount.countCharacters("Llanfairpwllgwyngyllgogerychwyrndrobwllllantysiliogogogoch"));
		assertEquals(5, wordCount.countCharacters("Cymru"));
//		assertEquals(3, wordCount.countCharacters("Tŷr"));
//		assertEquals(7, wordCount.countCharacters("Glyndŵr"));
	}

}
