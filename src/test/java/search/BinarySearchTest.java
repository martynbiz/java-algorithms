package search;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class BinarySearchTest {

    @Test
    public void testBinarySearch() {
    	
    	BinarySearch engine = new BinarySearch(); // MyClass is tested
    	
    	int array[] = {1,2,3,4,5,6,7,8,9};

        // assert statements
        assertEquals(-1, engine.search(array, 10), "Item not found");
        assertEquals(0, engine.search(array, 1), "Item not found");
        assertEquals(8, engine.search(array, 9), "Item not found");
        assertEquals(4, engine.search(array, 5), "Item not found");
    }
}
