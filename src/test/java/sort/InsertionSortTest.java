package sort;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import sort.InsertionSort;

class InsertionSortTest {

	@Test
	void test() {

		InsertionSort engine = new InsertionSort(); // MyClass is tested
    	
    	int array[] = {4,6,3,5,2,8,9,1,7};
    	int sorted[] = engine.ascending(array);

        // assert statements
        assertEquals(1, sorted[0], "Item not found");
        assertEquals(2, sorted[1], "Item not found");
        assertEquals(3, sorted[2], "Item not found");
        assertEquals(4, sorted[3], "Item not found");
        assertEquals(5, sorted[4], "Item not found");
        assertEquals(6, sorted[5], "Item not found");
        assertEquals(7, sorted[6], "Item not found");
        assertEquals(8, sorted[7], "Item not found");
        assertEquals(9, sorted[8], "Item not found");
	}

}
