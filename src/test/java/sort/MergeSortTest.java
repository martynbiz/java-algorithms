package sort;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class MergeSortTest {

	@Test
	void test() {

		MergeSort engine = new MergeSort(); // MyClass is tested
    	
    	int array[] = {4,6,3,5,2,8,9,1,7};
    	engine.sort(array);

        // assert statements
        assertEquals(1, array[0], "Item not found");
        assertEquals(2, array[1], "Item not found");
        assertEquals(3, array[2], "Item not found");
        assertEquals(4, array[3], "Item not found");
        assertEquals(5, array[4], "Item not found");
        assertEquals(6, array[5], "Item not found");
        assertEquals(7, array[6], "Item not found");
        assertEquals(8, array[7], "Item not found");
        assertEquals(9, array[8], "Item not found");
	}
	
}
